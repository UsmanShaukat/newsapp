//
//  NewsController.swift
//  NewsAppUIKit
//
//  Created by Kamran Shaukat on 29/1/22.
//

import UIKit

class NewsController: UIViewController {

    override func viewDidLoad() {
        
        super.viewDidLoad()

        let newsControllerView = NewsControllerView(frame: self.view.frame)
        
        self.view.addSubview(newsControllerView)
        
        self.title = "News App"

    }
    
    
}
