//
//  NewsControllerView.swift
//  NewsAppUIKit
//
//  Created by Kamran Shaukat on 29/1/22.
//

import UIKit

class NewsControllerView: UIView {

    let refreshControl = UIRefreshControl()
    let newsTableView = UITableView()
    var newsItems: [NewsItem] = []
    
    override func draw(_ rect: CGRect) {
                
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        setupTableView()
        
        //getNewsItems()
        
        addSubview(newsTableView)

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func refresh(_ sender: AnyObject) {
        newsItems.removeAll()
        newsTableView.reloadData()
        getNewsItems()
    }

    func setupTableView(){
        newsTableView.delegate = self
        newsTableView.dataSource = self
        newsTableView.frame = frame
        newsTableView.register(UITableViewCell.self, forCellReuseIdentifier: "TableViewCell")
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        newsTableView.addSubview(refreshControl)
    }
    
    func getNewsItems(){
        weak var weakSelf = self
        NewsItem.getAllNews(callBack: { (news:[NewsItem], error: Error?) in
            if error == nil{
                weakSelf?.newsItems = news
                weakSelf?.newsTableView.reloadData()
                weakSelf?.refreshControl.endRefreshing()
            }
        })
    }
    
}

extension NewsControllerView: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 30;//newsItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath)

        if cell.detailTextLabel == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: "TableViewCell")
        }

        cell.textLabel?.text = "title"
        cell.detailTextLabel?.text = "description"
        cell.imageView?.image = UIImage(named: "BBCNews")
        
//        cell.textLabel?.text = newsItems[indexPath.row].newsTitle
//        cell.detailTextLabel?.text = newsItems[indexPath.row].newsDescription
//        cell.imageView?.image = UIImage(named: "BBCNews")
        
        return cell
        
    }
    
    
}
