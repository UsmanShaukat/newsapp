//
//  NewsItem.swift
//  NewsAppUIKit
//
//  Created by Kamran Shaukat on 29/1/22.
//

import UIKit
import Alamofire


class NewsItem: NSObject {

    var newsID : String? = ""
    var newsTitle : String? = ""
    var newsDescription : String? = ""

    class func getAllNews(callBack:  @escaping ([NewsItem], Error?) -> () ){
        
        let url = Helpers.appleAppStoreNewsAPI + Helpers.apiKey
                
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers : nil).responseJSON { response in
            
            if(response.result.value == nil){
                callBack([], nil)
            }
            else{
                let arr = parseNewsResponse(data: (response.result.value as? NSDictionary)!)
                callBack(arr, nil)
            }
            
        }
        
    }
    
    class func parseNewsResponse(data: NSDictionary) -> [NewsItem]{
        
        var newsItemsArray:[NewsItem] = []
        
        if let articlesArray = data["articles"] as? NSArray{
            for i in 0..<(articlesArray.count){
                let article = articlesArray[i] as! NSDictionary
                let newsItem = NewsItem()
                newsItem.newsTitle = article["title"] as? String ?? ""
                newsItem.newsDescription = article["content"] as? String ?? ""
                newsItemsArray.append(newsItem)
            }
            print(newsItemsArray)
        }
        
        return newsItemsArray
        
    }
    
    
}
