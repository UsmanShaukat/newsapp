//
//  Helpers.swift
//  NewsAppUIKit
//
//  Created by Kamran Shaukat on 29/1/22.
//

import Foundation


class Helpers{

    static let apiKey : String = "6241ff173ed8493781853f707fecf635"
    
    static let appleAppStoreNewsAPI : String = "https://newsapi.org/v2/everything?q=apple&from=2022-01-11&to=2022-01-11&sortBy=popularity&apiKey="

}
