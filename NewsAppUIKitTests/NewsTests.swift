//
//  NewsTests.swift
//  NewsAppUIKitTests
//
//  Created by Kamran Shaukat on 29/1/22.
//

import XCTest
@testable import NewsAppUIKit

class NewsTests: XCTestCase {

    var newsItem: NewsItem!
    
    override func setUpWithError() throws {

    }

    override func tearDownWithError() throws {

    }

    func testNewsData() throws {
        
        newsItem.newsTitle = "anyTitle"
        
        newsItem.newsDescription = "anyDescription"
        
        XCTAssertNotNil(newsItem)

        XCTAssertTrue(newsItem.newsDescription == "", "Description should not be empty")
        
        XCTAssertTrue(newsItem.newsTitle == "", "Title should not be empty")

        XCTAssertTrue(newsItem.newsTitle == newsItem.newsDescription, "Title and Description cannot be equal")

    }

    func testPerformanceExample() throws {

        self.measure {

        }

    }

}
